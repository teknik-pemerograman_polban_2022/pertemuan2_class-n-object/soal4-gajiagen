import java.util.Scanner;

public class App {

    /**
     * Main program
     * 
     * @param args unused
     * @throws Exception unused
     */
    public static void main(String[] args) throws Exception {
        float Gaji = 500000;
        int HargaItem = 50000;

        Scanner kScanner = new Scanner(System.in);
        int JumlahTerjual = kScanner.nextInt();
        float bonus = 0;

        if (JumlahTerjual < 15) {

            bonus = 15;
            Gaji = Gaji - ((HargaItem * (15 - JumlahTerjual)) * (bonus / 100));
        } else if (JumlahTerjual < 40) {

            bonus = 10;
            Gaji = Gaji + ((HargaItem * (bonus / 100)) * JumlahTerjual);
        } else {

            if (JumlahTerjual >= 40) {
                bonus = 25;
            } else if (JumlahTerjual >= 80) {
                bonus = 35;
            }

            Gaji = Gaji + ((HargaItem * JumlahTerjual) * (bonus / 100));

        }

        System.out.println(Gaji);
        kScanner.close();
    }
}
